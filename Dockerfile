FROM ubuntu:22.04

COPY ./*.deb /tmp/

RUN apt update && dpkg -i /tmp/*.deb && rm -rf /tmp/*.deb