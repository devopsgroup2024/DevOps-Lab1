#include <iostream>
#include <stdlib.h>
#include "factorialio.h"

int main(int argc, char* argv[])
{
	if(argc > 2)
	{
		std::cout << "Некорректный ввод\n\n";
		return -1;
	}

	if(argc == 2)
	{
		int num = atoi(argv[1]);

		if(num == 0 && argv[1][0] != '0') //Проверка, что пользователь ввел ноль, а не текст
		{
			std::cout << "Некорректный ввод\n\n";
			return -1;
		}
		if (num >= 0 && num <= 20)
		{
			std::cout << "Факториал равен " << FindFact(num) << "\n\n";
			return 0;
		}
		else
		{
			std::cout << "Некорректный ввод\n\n";
			return -1;
		}
	}

	int input = 1;
	while (input != -1) // Меню программы
	{
		std::cout << "Введите целое положительное число от 0 до 20 или -1 для выхода\n";
		input = GetInput();
		if (input >= 0 && input <= 20)
			std::cout << "Факториал равен " << FindFact(input) << "\n\n";
		else if (input != -1)
			std::cout << "Некорректный ввод\n\n";
	}
	std::cout << "До свидания\n";
	return 0;
}