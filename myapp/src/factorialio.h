#ifndef FACTORIALIO_H 
#define FACTORIALIO_H

#include <iostream>
#include <string>
#include <sstream>

size_t FindFact(int num) // Вычисление факториала
{
	size_t fact;
	for (fact = 1; num > 1; num--)
		fact *= num;
	return fact;
}

int GetInput() // Получение и проверка введеного значения
{
	int intInput = 0;
	std::string input;
	
	// Получение введеного значения
	getline(std::cin, input, '\n');

	std::istringstream is(input);
	is >> intInput;

	// Проверка введеного значения
	if (!is.fail()) // Проверка пройдена
	{
		return intInput;
	}

	return -2; // Проверка не пройдена
}

#endif //FACTORIALIO_H
