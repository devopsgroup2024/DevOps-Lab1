#include "factorialio.h"
#include <cassert>

void TestingFact()
{
    assert(FindFact(5) == 120);
    assert(FindFact(8) == 40320);
    assert(FindFact(13) == 6227020800);
    assert(FindFact(0) == 1);
    std::cout << "TestingFact is completed" << std::endl;
}

int main()
{
    TestingFact();
    return 0;
}